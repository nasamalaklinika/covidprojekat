const LocalStrategy = require('passport-local').Strategy
const bcrypt = require('bcrypt')
function initialize(passport, getUserByEmail){
  const authenticateUser = (email, password, done) => {
    const user = getUserByEmail(email)
    if(user==null){
      return done(err, false, { message: 'Ne obstaja uporabnik s tim emailom'})
    }
    try{
      if(await bcrypt.compare(password), user.password){
        return done(err, user)
      }
      else{
        return done(err, false, { message: 'Napacno geslo'})
      }
    } catch(e){
      return done(e)
    }
  }

  passport.use(new LocalStrategy({ usernameField: 'email'}), authenticateUser)
  passport.serializeUser((user, done) => {   })
  passport.deserializeUser((id, done) => {   })
}
module.exports = initialize