const express = require('express');
const path = require('path');
const session = require('express-session');
//fajl za konektovanje sa bazom
const db=require('./connection');
//definisanje rutera 
var bodyParser = require('body-parser');
const indexRouter = require('./routes/index');
const app = express();
const upload = require("express-fileupload");
const Bolezeni=require('./models/bolezni');
const User=require('./models/user');
const Clanek=require('./models/clanek')

app.use(upload());



app.use(session({
	secret: 'secret',
	resave: true,
	saveUninitialized: true
}));

app.use(function(req, res, next) {
  res.locals.user = req.session.userid;
  next();
});

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

//definisanje porta 
const PORT = process.env.PORT || 3000

//namestitev foldera za renderovanje stranica
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.engine('html', require('ejs').renderFile);


const moment = require("moment");

app.use((req, res, next)=>{
    res.locals.moment = moment;
    next();
  });

//folder za shranjivanje staticnih elementov
app.use(express.static(path.join(__dirname, '/public')));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));


//uporabljamo ruter za preusmeritev na homepage v ovem primeru
app.use('/', indexRouter);
app.use('/register', indexRouter);

app.post("/upload",function(req,res){
  if(req.files){
    var id=req.body.bolezen;
    var covid=req.body.covid;
    console.log("request");
    var file = req.files.file;
    file.mv(`${__dirname}/uploads/${file.name}`,function(err){
      if(err){
        console.log(err)
        res.send("error occured")
      }
      else{
        Bolezeni.potvrdi(id, (err, data) => {
          if (err)
            res.status(500).send({
              message: 'Error'
            });
          else
            console.log("Dijagnoza potvrdjena");
      
        });
        if (covid == 'covid19' || covid == 'COVID19' || covid == 'COVID-19' || covid == 'covid-19' || covid == 'Covid-19')
        {
          User.okruzbo(user,(err, stats) => {
            if (err)
              res.status(500).send({
                message:
                  "Failed to update okruzba"
              });
          });

          User.okruzbo1(user,(err, stats) => {
            if (err)
              res.status(500).send({
                message:
                  "Failed to update okruzba"
              });
          });
        }
        res.redirect("/koledar")
      }
    })
  }

});


app.post('/dodaj', async (req, res, next) => {
 
  console.log("request");
  if(req.files){
    console.log("request");
    var file = req.files.file;
    file.mv(`${__dirname}/public/images/${file.name}`,function(err){
      if(err){
        console.log(err)
        res.send("error occured")
      }
      else
      {
        let nov_clanek = {
          Image:`/images/${file.name}`,
          Naziv: req.body.Naziv,
          Opis: req.body.Opis,
          Datum: new Date().toISOString().slice(0, 19).replace('T', ' '),
          Uporabnik_idUporabnik: 1
        }
        Clanek.dodaj(nov_clanek, (err, data) => {
          if (err)
            res.status(500).send({
              message: "Failed to add clanek"
            });
          else
            res.redirect('/admin');
        })
      }
    })
  }

  

});


//izmenjava  clanka za admina 
app.post('/edit/:idClanek', async (req, res, next) => {
  var idClanek = req.params.idClanek;
  
  if(req.files){
    
    console.log("request");
    var file = req.files.file;
    file.mv(`${__dirname}/public/images/${file.name}`,function(err){
      if(err){
        console.log(err)
        res.send("error occured")
      }
      else
      {

        let podaci = {
          Image:`/images/${file.name}`,
          Naziv: req.body.Naziv,
          Opis: req.body.Opis
        }
       
        Clanek.izmeni2(idClanek,podaci, (err, data) => {
          if (err)
            res.status(500).send({
              message: "Failed to add clanek"
            });
          else
            res.redirect('/admin');
        })
      }
    })
  }
else
{
  let podaci = {
    Naziv: req.body.Naziv,
    Opis: req.body.Opis
  }

  Clanek.izmeni(idClanek, podaci, (err, data) => {
    if (err)
      res.status(500).send({
        message: "Failed to update clanek"
      });
    else
      res.redirect('/admin');
  })
}
}); 

app.listen(3000, '127.0.0.1');
//app startuje server na portu kateri smo definisali
//app.listen(PORT, () =>
//{
console.log("Server running on port "+PORT);
//});