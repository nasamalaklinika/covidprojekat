//konekcija sa podatkovnom bazom
const mysql=require('mysql');
const db=mysql.createConnection(
    {
      host:'localhost',
      user:'root',
      password:'password',
      database:'mydb',
      port:'3306'
    }
  )
  
  db.connect((err) => {
    if(err){
      console.log('Error connecting to Db');
      return;
    }
    console.log('Connection established');
  });

  db.on('error', function(err) {
    console.log("[mysql error]",err);
  });
  
  
  module.exports=db;
