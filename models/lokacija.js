var sql = require('../connection');

var Lokacija = function(lokacija) {
    this.Lat = lokacija.Lat;
    this.Lng = lokacija.Lng;
};

Lokacija.dodaj = function (lokacija, result) {    
    sql.query("INSERT INTO Lokacija set ?", lokacija, function (err, res) {
            
            if(err) {
                console.log("error: ", err);
                result(err, null);
            }
            else{
                console.log(res.insertId);
                result(null, res.insertId);
            }
        });         
};

Lokacija.beri = function (lokacija, result) {
    sql.query('SELECT * from Lokacija', function (err, res) {             
            if(err) {
                console.log("error: ", err);
                result(err, null);
            }
            else{
                result(null, res);
          
            }
        });   
};

module.exports=Lokacija;