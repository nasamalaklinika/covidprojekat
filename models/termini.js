var sql = require('../connection');

var Termin = function(Termin){
    this.Datum=Datum;
    this.Cas=Cas;
    this.Lokacija_idLokacija=Lokacija_idLokacija;
};


Termin.get = function (termin, result) {
        sql.query('SELECT idTermini, idLokacija, mydb.lokacija.Naslov, mydb.lokacija.Naziv, mydb.termini.Datum, mydb.termini.Cas, (6371 * acos(cos(radians(?)) *  cos(radians(Lat)) * cos(radians(Lng) - radians(?)) + sin(radians(?)) *  sin(radians(Lat))) ) AS distance  FROM mydb.lokacija, mydb.termini WHERE idLokacija=mydb.termini.Lokacija_idLokacija and mydb.termini.Datum>curdate() ORDER BY distance,mydb.termini.Datum,mydb.termini.Cas  LIMIT 0,1; ',[termin.Lat,termin.Lng,termin.Lat], function (err, res) {             
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else{
                    console.log(res);
                    result(null, res);
              
                }
            });   
};


Termin.izbrisi = function(id, result){
    sql.query("DELETE FROM mydb.termini WHERE idTermini = ?", [id], function (err, res) {

               if(err) {
                   console.log("error: ", err);
                   result(null, err);
               }
               else{
              
                result(null, res);
               }
           }); 
};


module.exports= Termin;

