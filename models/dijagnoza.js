var sql = require('../connection');

var Dijagnoza = function(dijagnoza){
    this.Naziv = dijagnoza.Naziv;
    this.Opis = dijagnoza.Opis;
    this.Zdravljenje = dijagnoza.Zdravljenje;
    this.Datum = new Date();  
};

Dijagnoza.dodaj = function (dijagnoza, result) {    
    sql.query("INSERT INTO Dijagnoza set ?", dijagnoza, function (err, res) {
            
            if(err) {
                console.log("error: ", err);
                result(err, null);
            }
            else{
                console.log(res.insertId);
                result(null, res.insertId);
            }
        });  
        

};

module.exports=Dijagnoza;