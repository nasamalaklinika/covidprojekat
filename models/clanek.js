var sql = require('../connection');

var Clanek = function(clanek){
    this.Image = clanek.Image;
    this.Naziv = clanek.Naziv;
    this.Opis = clanek.Opis;
    this.Datum = new Date().toISOString().slice(0, 19).replace('T', ' ');
    this.Uporabnik_idUporabnik=1;
};



Clanek.dodaj = function (nov_clanek, result) {    
        sql.query("INSERT INTO clanek set ?", nov_clanek, function (err, res) {
                
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else{
                    console.log(res);
                    result(null, res);
                }
            });           
};
Clanek.get = function (idClanek, result) {
        sql.query('SELECT * FROM clanek WHERE idClanek = ?', [idClanek], function (err, res) {             
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else{
                    result(null, res);
              
                }
            });   
};
Clanek.beri = function (result) {
        sql.query("Select * from clanek ORDER BY Datum desc", function (err, res) {

                if(err) {
                    console.log("error: ", err);
                    result(null, err);
                }
                else{
                  console.log('tasks : ', res);  

                 result(null, res);
                }
            });   
};
Clanek.izmeni = function(id, podaci, result){
  sql.query("UPDATE clanek SET Naziv = ? ,Opis = ?, Datum = ? WHERE idClanek = ?", [podaci.Naziv, podaci.Opis, new Date().toISOString().slice(0, 19).replace('T', ' ')
  , id], function (err, res) {
          if(err) {
              console.log("error: ", err);
                result(null, err);
             }
           else{   
             result(null, res);
                }
            }); 
};

Clanek.izmeni2 = function(id, podaci, result){
    sql.query("UPDATE clanek SET Naziv = ? ,Opis = ?,  Image= ?, Datum = ? WHERE idClanek = ?", [podaci.Naziv, podaci.Opis,podaci.Image, new Date().toISOString().slice(0, 19).replace('T', ' ')
    , id], function (err, res) {
            if(err) {
                console.log("error: ", err);
                  result(null, err);
               }
             else{   
               result(null, res);
                  }
              }); 
  };

Clanek.izbrisi = function(id, result){
     sql.query("DELETE FROM clanek WHERE idClanek = ?", [id], function (err, res) {

                if(err) {
                    console.log("error: ", err);
                    result(null, err);
                }
                else{
               
                 result(null, res);
                }
            }); 
};
module.exports= Clanek;
