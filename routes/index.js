const Clanek = require("../models/clanek");
const User = require("../models/user");
const Dijagnoza = require("../models/dijagnoza.js");
const Simptome = require("../models/simptome.js");
const Statistika = require("../models/statistika");
const Bolezni = require("../models/bolezni.js");
//const Chart = require("../models/chart");
const podaciChart = require("../models/podaciChart");
const Lokacija = require("../models/lokacija");
var sql = require('../connection');
var async = require('async');
const Termin = require("../models/termini");


var express = require('express');
const db = require('../connection');
var router = express.Router();
router.use(express.static('views'));

router.get('/register', function (req, res, next) {
  res.render('register.html');
});

//registriranje novih korisnika
router.post('/register', function (req, res, next) {
  var uporabnik=req.body.uporabnisko_ime;
  var email=req.body.inputEmail;
  let nov_user = {
    uporabnisko_ime: req.body.uporabnisko_ime,
    geslo: req.body.password,
    ime: req.body.name,
    priimek: req.body.surname,
    email: req.body.inputEmail,
    datum_rojstva: req.body.datum_rojstva,
    isAdmin: 0
  }
if (req.body.password==req.body.password1)
{
  User.check(uporabnik,email ,(err, data) => {
    if (data!=null)
    {
    User.dodaj(nov_user, (err, data) => {
      if (err)
        res.redirect('/register');
    });
    res.redirect('/login');
}
else
{
  res.redirect('/register');

}
  
  });
}
else
{
  res.redirect('/register');

}
});

//prikaz svih clankov za admina
router.get('/admin', function (req, res, next) {
  Clanek.beri((err, data) => {
    if (err)
      res.status(500).send({
        message: "Cant get admin page"
      });
    else
      res.render('admin.html', {
        data: data
      });
  });
});

router.get('/login', function (req, res, next) {
  res.render('login.html');
});


router.get('/logout', function (req, res, next) {
  req.session.destroy();
  res.redirect('/');
});

//login za korisnike
router.post('/login', function (req, res, next) {

  var uporabnisko_ime = req.body.uporabnisko_ime;
  var geslo = req.body.password;
  let result;
  User.get(uporabnisko_ime, geslo, (err, data) => {
    if (data == null)
      res.redirect('/login');
    else {
      req.session.user = uporabnisko_ime;
      req.session.userid = data[0].idUporabnik;
      req.session.email = data[0].email;
      console.log(req.session.user);
      if (data[0].isAdmin == 1)
        res.redirect('/admin');
      else
        res.redirect('/');
    }
  })


});


router.post('/okruzba/:idprijatelj', function(req, res, next) {
  var user=req.session.userid;
  var idprijatelj = req.params.idprijatelj;
  if(user==null)
  {
    res.redirect('/login');
  }
  else {

    console.log(idprijatelj)
    User.zbrisi_okruzbo(idprijatelj,user, (err, data) => {
      if (err)
        res.status(500).send({
          message:
            "Failed to load friends"
        });
      
     
    });

    User.zbrisi_okruzbo(user,idprijatelj, (err, data) => {
      if (err)
        res.status(500).send({
          message:
            "Failed to load friends"
        });
      
    });

    User.zbrisi_okruzbo(idprijatelj,user, (err, data) => {
      if (err)
        res.status(500).send({
          message:
            "Failed to load friends"
        });
     
    
    });
    res.redirect('/prijatelji');
  }
});

router.get('/prijatelji', function (req, res, next) {
  user = req.session.userid;
  if (user == null) {
    res.redirect('/login');
  } else {
    User.prijatelji(user, (err, data) => {
      if (err)
        res.status(500).send({
          message: "Failed to load friends"
        });
      else
        res.render('prijatelji.html', {
          data: data
        });
    });
  }
});


//dodavanje prijatelja
router.post('/prijatelj/dodaj', function (req, res, next) {
  var user = req.session.userid;
  var email = req.body.email;
  var friend;

  User.get_prijatelj(user, email, (err, data) => {
    if (data == null)
      res.redirect('/prijatelji');
   else {
      friend=data[0].idUporabnik;
    User.preveri(user,friend, (err, data) => {
      if (data != null)
      User.prijatelj_dodaj(user,friend, (err, data)=> {
        res.redirect('/prijatelji');
      });
      res.redirect('/prijatelji');
    })
    

  }
})
});



router.post('/delete/:Uporabnik_idUporabnik1', function(req, res, next) {
  let uporabnik1 = req.params.Uporabnik_idUporabnik1;
  let uporabnik2 = req.session.userid;
  User.izbrisi_prijatelja(uporabnik1, uporabnik2, (err, data) => {
    if (err)
      res.status(500).send({
        message: "Error deleting friend"
      });
    else
      res.redirect('/prijatelji');
  });
});


// router.get('/statistika', function (req, res, next) {
//   res.render('statistika.html');
// });

router.get('/statistika', function (req, res, next) {
  res.render('statistika.html',data=null,option=null);
});

router.post('/mesec', function (req, res, next) {
  let data = new Array();
  Leto = req.body.Leto;
  async.parallel([
    function (callback) {
      sql.query('SELECT COUNT(idBolezni) as broj FROM bolezni, dijagnoza WHERE bolezni.Dijagnoza_idDijagnoza = dijagnoza.idDijagnoza AND dijagnoza.Naziv = \"Covid-19\" AND MONTH(Datum_Zacetka) = 1 AND YEAR(Datum_Zacetka) = ? ', [Leto], function (err, rows1) {
        if (err) {
          return callback(err);
        }
        return callback(null, rows1);
      });
    },
    function (callback) {
      sql.query('SELECT COUNT(idBolezni) as broj FROM bolezni, dijagnoza WHERE bolezni.Dijagnoza_idDijagnoza = dijagnoza.idDijagnoza AND dijagnoza.Naziv = \"Covid-19\" AND MONTH(Datum_Zacetka) = 2 AND YEAR(Datum_Zacetka) = ? ', [Leto], function (err, rows2) {
        if (err) {
          return callback(err);
        }
        return callback(null, rows2);
      });
    },
    function (callback) {
      sql.query('SELECT COUNT(idBolezni) as broj FROM bolezni, dijagnoza WHERE bolezni.Dijagnoza_idDijagnoza = dijagnoza.idDijagnoza AND dijagnoza.Naziv = \"Covid-19\" AND MONTH(Datum_Zacetka) = 3 AND YEAR(Datum_Zacetka) = ? ', [Leto], function (err, rows3) {
        if (err) {
          return callback(err);
        }
        return callback(null, rows3);
      });
    },
    function (callback) {
      sql.query('SELECT COUNT(idBolezni) as broj FROM bolezni, dijagnoza WHERE bolezni.Dijagnoza_idDijagnoza = dijagnoza.idDijagnoza AND dijagnoza.Naziv = \"Covid-19\" AND MONTH(Datum_Zacetka) = 4 AND YEAR(Datum_Zacetka) = ? ', [Leto], function (err, rows4) {
        if (err) {
          return callback(err);
        }
        return callback(null, rows4);
      });
    },
    function (callback) {
      sql.query('SELECT COUNT(idBolezni) as broj FROM bolezni, dijagnoza WHERE bolezni.Dijagnoza_idDijagnoza = dijagnoza.idDijagnoza AND dijagnoza.Naziv = \"Covid-19\" AND MONTH(Datum_Zacetka) = 5 AND YEAR(Datum_Zacetka) = ? ', [Leto], function (err, rows5) {
        if (err) {
          return callback(err);
        }
        return callback(null, rows5);
      });
    },
    function (callback) {
      sql.query('SELECT COUNT(idBolezni) as broj FROM bolezni, dijagnoza WHERE bolezni.Dijagnoza_idDijagnoza = dijagnoza.idDijagnoza AND dijagnoza.Naziv = \"Covid-19\" AND MONTH(Datum_Zacetka) = 6 AND YEAR(Datum_Zacetka) = ? ', [Leto], function (err, rows6) {
        if (err) {
          return callback(err);
        }
        return callback(null, rows6);
      });
    },
    function (callback) {
      sql.query('SELECT COUNT(idBolezni) as broj FROM bolezni, dijagnoza WHERE bolezni.Dijagnoza_idDijagnoza = dijagnoza.idDijagnoza AND dijagnoza.Naziv = \"Covid-19\" AND MONTH(Datum_Zacetka) = 7 AND YEAR(Datum_Zacetka) = ? ', [Leto], function (err, rows7) {
        if (err) {
          return callback(err);
        }
        return callback(null, rows7);
      });
    },
    function (callback) {
      sql.query('SELECT COUNT(idBolezni) as broj FROM bolezni, dijagnoza WHERE bolezni.Dijagnoza_idDijagnoza = dijagnoza.idDijagnoza AND dijagnoza.Naziv = \"Covid-19\" AND MONTH(Datum_Zacetka) = 8 AND YEAR(Datum_Zacetka) = ? ', [Leto], function (err, rows8) {
        if (err) {
          return callback(err);
        }
        return callback(null, rows8);
      });
    },
    function (callback) {
      sql.query('SELECT COUNT(idBolezni) as broj FROM bolezni, dijagnoza WHERE bolezni.Dijagnoza_idDijagnoza = dijagnoza.idDijagnoza AND dijagnoza.Naziv = \"Covid-19\" AND MONTH(Datum_Zacetka) = 9 AND YEAR(Datum_Zacetka) = ? ', [Leto], function (err, rows9) {
        if (err) {
          return callback(err);
        }
        return callback(null, rows9);
      });
    },
    function (callback) {
      sql.query('SELECT COUNT(idBolezni) as broj FROM bolezni, dijagnoza WHERE bolezni.Dijagnoza_idDijagnoza = dijagnoza.idDijagnoza AND dijagnoza.Naziv = \"Covid-19\" AND MONTH(Datum_Zacetka) = 10 AND YEAR(Datum_Zacetka) = ? ', [Leto], function (err, rows10) {
        if (err) {
          return callback(err);
        }
        return callback(null, rows10);
      });
    },
    function (callback) {
      sql.query('SELECT COUNT(idBolezni) as broj FROM bolezni, dijagnoza WHERE bolezni.Dijagnoza_idDijagnoza = dijagnoza.idDijagnoza AND dijagnoza.Naziv = \"Covid-19\" AND MONTH(Datum_Zacetka) = 11 AND YEAR(Datum_Zacetka) = ? ', [Leto], function (err, rows11) {
        if (err) {
          return callback(err);
        }
        return callback(null, rows11);
      });
    },
    function (callback) {
      sql.query('SELECT COUNT(idBolezni) as broj FROM bolezni, dijagnoza WHERE bolezni.Dijagnoza_idDijagnoza = dijagnoza.idDijagnoza AND dijagnoza.Naziv = \"Covid-19\" AND MONTH(Datum_Zacetka) = 12 AND YEAR(Datum_Zacetka) = ? ', [Leto], function (err, rows12) {
        if (err) {
          return callback(err);
        }
        return callback(null, rows12);
      });
    }
  ], function (error, callbackResults) {
    if (error) {
      //handle error
      console.log(error);
    } else {
      console.log(callbackResults[0]);
      console.log(callbackResults[1]);
      console.log(callbackResults[2]);
      console.log(callbackResults[3]);
      console.log(callbackResults[4]);
      console.log(callbackResults[5]);
      console.log(callbackResults[6]);
      console.log(callbackResults[7]);
      console.log(callbackResults[8]);
      console.log(callbackResults[9]);
      console.log(callbackResults[10]);
      console.log(callbackResults[11]);
      res.render('statistika.html', {
        data: callbackResults
      });
    }
  });
});




router.post('/leto', function (req, res, next) {
  let data = new Array();
  async.parallel([
    function (callback) {
      sql.query("SELECT COUNT(idBolezni) as broj FROM bolezni, dijagnoza WHERE bolezni.Dijagnoza_idDijagnoza = dijagnoza.idDijagnoza && dijagnoza.Naziv = \"Covid-19\" && YEAR(Datum_Zacetka) = 2018", function (err, rows1) {
        if (err) {
          return callback(err);
        }
        return callback(null, rows1);
      });
    },
    function (callback) {
      sql.query("SELECT COUNT(idBolezni) as broj FROM bolezni, dijagnoza WHERE bolezni.Dijagnoza_idDijagnoza = dijagnoza.idDijagnoza && dijagnoza.Naziv = \"Covid-19\" && YEAR(Datum_Zacetka) = 2019", function (err, rows2) {
        if (err) {
          return callback(err);
        }
        return callback(null, rows2);
      });
    },
    function (callback) {
       sql.query("SELECT COUNT(idBolezni) as broj FROM bolezni, dijagnoza WHERE bolezni.Dijagnoza_idDijagnoza = dijagnoza.idDijagnoza && dijagnoza.Naziv = \"Covid-19\" && YEAR(Datum_Zacetka) = 2020",  function (err, rows3) {
        if (err) {
          return callback(err);
        }
        return callback(null, rows3);
      });
    }
  ], function (error, callbackResults) {
    if (error) {
      //handle error
      console.log(error);
    } else {
      console.log(callbackResults[0]);
      console.log(callbackResults[1]);
      console.log(callbackResults[2]);
      res.render('statistika.html', {
        data: callbackResults
      });

    }
  });
});


router.post('/mesecbolezen', function (req, res, next) {
  mesec = req.body.Mesec;
  leto = req.body.Leto;
  let data = new Array();
  async.parallel([
    function (callback) {
      sql.query("SELECT COUNT(idBolezni) as broj FROM bolezni, dijagnoza WHERE bolezni.Dijagnoza_idDijagnoza = dijagnoza.idDijagnoza && dijagnoza.Naziv = \"Grip\" && MONTH(Datum_Zacetka) = ? && YEAR(Datum_Zacetka) = ?",[mesec, leto], function (err, rows1) {
        if (err) {
          return callback(err);
        }
        return callback(null, rows1);
      });
    },
    function (callback) {
      sql.query("SELECT COUNT(idBolezni) as broj FROM bolezni, dijagnoza WHERE bolezni.Dijagnoza_idDijagnoza = dijagnoza.idDijagnoza && dijagnoza.Naziv = \"Alergija\" && MONTH(Datum_Zacetka) = ? && YEAR(Datum_Zacetka) = ?",[mesec, leto], function (err, rows2) {
        if (err) {
          return callback(err);
        }
        return callback(null, rows2);
      });
    },
    function (callback) {
       sql.query("SELECT COUNT(idBolezni) as broj FROM bolezni, dijagnoza WHERE bolezni.Dijagnoza_idDijagnoza = dijagnoza.idDijagnoza && dijagnoza.Naziv = \"Prehlada\" && MONTH(Datum_Zacetka) = ? && YEAR(Datum_Zacetka) = ?",[mesec, leto],  function (err, rows3) {
        if (err) {
          return callback(err);
        }
        return callback(null, rows3);
      });
    },
    function (callback) {
      sql.query("SELECT COUNT(idBolezni) as broj FROM bolezni, dijagnoza WHERE bolezni.Dijagnoza_idDijagnoza = dijagnoza.idDijagnoza && dijagnoza.Naziv = \"Covid-19\" && MONTH(Datum_Zacetka) = ? && YEAR(Datum_Zacetka) = ?", [mesec, leto],  function (err, rows4) {
       if (err) {
         return callback(err);
       }
       return callback(null, rows4);
     });
   }
  ], function (error, callbackResults) {
    if (error) {
      //handle error
      console.log(error);
    } else {
      console.log(callbackResults[0]);
      console.log(callbackResults[1]);
      console.log(callbackResults[2]);
      console.log(callbackResults[3]);
      res.render('statistika.html', {
        data: callbackResults
      });

    }
  });
});


router.post('/bolezniuporabnik', function (req, res, next) {
  id = req.session.userid;
  leto = req.body.Leto;
  let data = new Array();
  async.parallel([
    function (callback) {
      sql.query("SELECT COUNT(idBolezni) as broj FROM bolezni, uporabnik, dijagnoza WHERE bolezni.Dijagnoza_idDijagnoza = dijagnoza.idDijagnoza && bolezni.Uporabnik_idUporabnik = uporabnik.idUporabnik && dijagnoza.Naziv = \"Grip\" && YEAR(Datum_Zacetka) = ? && uporabnik.idUporabnik = ?",[leto, id], function (err, rows1) {
        if (err) {
          return callback(err);
        }
        return callback(null, rows1);
      });
    },
    function (callback) {
      sql.query("SELECT COUNT(idBolezni) as broj FROM bolezni, uporabnik, dijagnoza WHERE bolezni.Dijagnoza_idDijagnoza = dijagnoza.idDijagnoza && bolezni.Uporabnik_idUporabnik = uporabnik.idUporabnik && dijagnoza.Naziv = \"Alergija\" && YEAR(Datum_Zacetka) = ? && uporabnik.idUporabnik = ?",[leto, id], function (err, rows2) {
        if (err) {
          return callback(err);
        }
        return callback(null, rows2);
      });
    },
    function (callback) {
       sql.query("SELECT COUNT(idBolezni) as broj FROM bolezni, uporabnik, dijagnoza WHERE bolezni.Dijagnoza_idDijagnoza = dijagnoza.idDijagnoza && bolezni.Uporabnik_idUporabnik = uporabnik.idUporabnik && dijagnoza.Naziv = \"Prehlada\" && YEAR(Datum_Zacetka) = ? && uporabnik.idUporabnik = ?",[leto, id],  function (err, rows3) {
        if (err) {
          return callback(err);
        }
        return callback(null, rows3);
      });
    },
    function (callback) {
      sql.query("SELECT COUNT(idBolezni) as broj FROM bolezni, uporabnik, dijagnoza WHERE bolezni.Dijagnoza_idDijagnoza = dijagnoza.idDijagnoza && bolezni.Uporabnik_idUporabnik = uporabnik.idUporabnik && dijagnoza.Naziv = \"Covid-19\" && YEAR(Datum_Zacetka) = ? && uporabnik.idUporabnik = ?", [leto, id],  function (err, rows4) {
       if (err) {
         return callback(err);
       }
       return callback(null, rows4);
     });
   }
  ], function (error, callbackResults) {
    if (error) {
      //handle error
      console.log(error);
    } else {
      console.log(callbackResults[0]);
      console.log(callbackResults[1]);
      console.log(callbackResults[2]);
      console.log(callbackResults[3]);
      res.render('statistika.html', {
        data: callbackResults
      });

    }
  });
});



router.post('/statistika', function (req, res, next) {
  podaci = req.body.Leto;

  podaciChart.Leto(podaci, (err, data) => {
    if (err || data == null) {
      message: "Failed to retrive all data from statistika";
      res.redirect('/statistika');
    }
    else
      res.redirect('/statistika');
  });
});


router.get('/vnos', function (req, res, next) {
  res.render('vnos.html');
});

router.get('/dijagnoza', function (req, res, next) {
  res.render('dijagnoza.html');
});

router.get('/koledar', function (req, res, next) {
  user=req.session.userid;
  var Uporabnik_id_Uporabnik=req.session.userid;
  if(user==null)
  {
    res.redirect('/login');
  }
  else
  {
    Bolezni.beri(Uporabnik_id_Uporabnik, (err, data) => {
      if (err)
        res.status(500).send({
          message:
            "Failed to get data"
        });
        else
      res.render('koledar.html', { data: data });
    });
  }
 
});


router.post('/izbrisi/:idBolezni', function(req, res, next) {
  let bolezen = req.params.idBolezni;
  Bolezni.izbrisi(bolezen,(err, data) => {
    if (err)
      res.status(500).send({
        message:
          "Napaka pri brisanje bolezen"
      });
    else
    res.redirect('/koledar');
  });
});



router.get('/dijagnoza/zdravljenje', function (req, res, next) {
  res.render('zdravljenje.html');
});

//prikaz domov sa statistikom
router.get('/', function (req, res, next) {
  Statistika.return((err, stats) => {
    if (err)
      res.status(500).send({
        message: "Failed to reach homepage"
      });
    else
      res.render('domov.html', {
        stats: stats
      });
  });

});


//citanje svih clankov iz podatkovne baze
router.get('/novice', function (req, res, next) {
  Clanek.beri((err, data) => {
    if (err)
      res.status(500).send({
        message: "Failed to get articales"
      });
    else
      res.render('novice.html', {
        data: data
      });
  });
});

//prikaz clanka za izmenjavu admin
router.get('/edit/:idClanek', async (req, res, next) => {
  let idClanek = req.params.idClanek;
  Clanek.get(idClanek, (err, data) => {
    if (err)
      res.status(500).send({
        message: 'Failed to get data from clanek'
      });
    else
      res.render('edit.html', {
        data: data
      });

  })
});

//brisanje clanka admin
router.post('/izbrisi_clanek/:idClanek', async (req, res, next) => {
  try {
    let idClanek = req.params.idClanek;
    Clanek.izbrisi(idClanek, (err, data) => {
      if (err) {
        res.status(404).send({
          message: `Failed to delete clanek with ${idClanek}.`

        });
      } else
        res.redirect('/admin')

    });
  } catch (error) {
    res.status(500).json(error);
  }
});

//prikaz stranice za dodavanje clanka admin
router.get('/dodaj', async (req, res, next) => {
  res.render('dodaj.html');
});

//dodavanje clanka  admin


//prikazivanje pojedinog clanka za korisnike 
router.get('/novice/:idClanek', async (req, res, next) => {
  var idClanek = req.params.idClanek;
  Clanek.get(idClanek, (err, data) => {
    if (err)
      res.status(500).send({
        message: 'Failed to get clanek data'
      });
    else
      res.render('post.html', {
        data: data
      });

  })
});


router.get('/statistika_admin', function (req, res, next) {
  Statistika.return((err, stats) => {
    if (err)
      res.status(500).send({
        message: "Failed to retrive all data from statistika for admin"
      });
    else
      res.render('izmeni_statistika.html', {
        stats: stats
      });
  });

});

router.post('/statistika_admin', function (req, res, next) {
  let podaci = {
    SteviloNovih: req.body.nov,
    SteviloPotrjenih: req.body.pot,
    SteviloOzdravljenih: req.body.ozd,
    SteviloUmrlih: req.body.umr
  }
  Statistika.izmeni(podaci, (err, stats) => {
    if (err)
      res.status(500).send({
        message: "Failed to change data from statistika"
      });
    else
      res.redirect('/admin');
  });

});



var bodyParser = require('body-parser');

var urlencodedParser = bodyParser.urlencoded({
  extended: false
});


router.post('/dijagnoza', async function (req, res) {

  var temperatura = req.body.temperaturo;
  var kaselj = req.body.kaselj;
  var kihanje = req.body.kihanje;
  var grlo = req.body.grlo;
  var glavobol = req.body.glavobol;
  var prsih = req.body.prsih;
  var izcedek = req.body.izcedek;
  var srbec= req.body.srbec;
  var zacetek = req.body.zacetek;
  var z = new Date(zacetek);
  var konec = req.body.konec;
  var k = new Date(konec);
  var user = req.session.userid;

  var trajanje;

  if (konec == undefined) {
    trajanje = 0;
  }

  if (kaselj === undefined) {
    kaselj = 0;
  }
  if (kihanje === undefined) {
    kihanje = 0;
  }
  if (grlo === undefined) {
    grlo = 0;
  }

  if (srbec === undefined) {
    srbec = 0;
  }

  if (glavobol === undefined) {
    glavobol = 0;
  }
  if (prsih === undefined) {
    prsih = 0;
  }
  if (izcedek === undefined) {
    izcedek = 0;
  }
  if (temperatura == '') {
    temperatura = 36;
  }


  try {
    let simptome = {
      Temperatura: temperatura,
      Kaselj: kaselj,
      Kihanje: kihanje,
      'Bolece grlo': grlo,
      Glavobol: glavobol,
      'Izcedek  iz  nosu': izcedek,
      'Bolecina v prsih': prsih,
       Trajanje: 10,
       'Srbeč nos/oči':srbec
    };


    Simptome.dodaj(simptome, (err, data) => {
      if (err)
        res.redirect('vnos');
      else {
        let bolezni = {
          PotrjenaDijagnoza: 0,
          Datum_Zacetka: z,
          Datum_Konca: k,
          Uporabnik_idUporabnik: user,
          Dijagnoza_idDijagnoza: 1,
          Simptomi_id_Simptomi: data
        }
       
        Bolezni.dodaj(bolezni, (err, data) => {
          if (err)
            res.status(500).send({
              message: "Failed to retrive all data from simptome"
            });
          else {

            console.log("dodane simptome");
            console.log("shranjen bolezen");
            
          }
        })
        res.redirect('dijagnoza/' + data);

      }
    })
  } catch (error) {
    res.status(500).json(error);
    console.log(error);
  }
});

router.get('/prikazsimptomi', (req, res) => {
  db.query('SELECT * FROM mydb.simptomi', (err, rows, fields) => {
    if (!err) {

      res.send(rows).json;
    } else
      res.status(500).send({
        message: "Failed to retrive all data from simptomi"
      });
  })
});


router.get('/prikazdijagnoza', (req, res) => {
  db.query('SELECT * FROM mydb.dijagnoza', (err, rows, fields) => {
    if (!err)
      res.json(rows);
    else
      res.sendStatus(404);
  })
});



router.get('/dijagnoza/:id_Simptomi', async (req, res, next) => {   try
  {
  let id_Simptomi = req.params.id_Simptomi;
  let id = req.params.id_Simptomi;
  console.log("error");
  Simptome.get(id_Simptomi, (err, data) => {
    if (err ||  data == null) {
    res.redirect('/vnos');
      }
    
    else {
    
      Lokacija.beri(id_Simptomi, (err, location) => {
        if (err ||  data == null) {
          res.status(404).send({
            message: `Failed to get simptomi with ${id_Simptomi}.`
  
          });
        }
    res.render('dijagnoza.html', {data: data,location:location});
      });
  }
});
    
    Bolezni.izmeni(id_Simptomi, (err, data) => {
      if (err)
        res.status(500).send({
          message:
            "Failed to update bolezni"
        });
      else
        console.log("shranjena dijagnoza");
    });
    console.log('prikaz simptome id = ' + id_Simptomi);


  }
  catch (error) {
    res.status(500).json(error);
  }
});

router.post('/dijagnoza/:id_Simptomi', async (req, res, next) => {
  try {
    let id_Simptomi = req.params.id_Simptomi;
    res.redirect('zdravljenje/' + id_Simptomi);
  } catch (error) {
    res.status(500).json(error);
  }
});


router.get('/dijagnoza/zdravljenje/:id_Simptomi', async (req, res, next) => {
  try {
    let id_Simptomi = req.params.id_Simptomi;
    Simptome.pridobi(id_Simptomi, (err, data) => {
      if (err) {
        res.status(404).send({
          message: `Failed to get simptomi with ${id_Simptomi}.`

        });
      } else {
        let naziv = JSON.stringify(data);
        console.log(naziv);
        res.render('zdravljenje.html', {
          data: data
        });
      }
    });
  } catch (error) {
    res.status(500).json(error);
  }
});

router.post('/termin', async (req, res, next) => {
  
  try {
    let lat = req.body.lat;
    let lon = req.body.lon;
    console.log(lat);
    console.log(lon);
    res.redirect('/termin/'+ lat+ '/'+ lon);
  } catch (error) {
    res.status(500).json(error);
  }
});


router.get('/termin/:lat/:lon', async (req, res, next) => {
  

  let lat = req.params.lat;
  let lon = req.params.lon;
  user=req.session.userid;
  
  try {
    let koordinate = {
      Lat: lon,
      Lng: lat
    }
    Termin.get(koordinate, (err, data) => {
      if (err) {
        res.status(404).send({
          message: `Failed to get simptomi with ${id_Simptomi}.`

        });
      } else {
        res.render('termin.html', {data:data});      }
    });

  } catch (error) {
    res.status(500).json(error);
  }
});

router.post('/brisi/:idTermin', function (req, res, next) {
  let ter = req.params.idTermin;
  Termin.izbrisi(ter, (err, data) => {
    if (err)
      res.status(500).send({
        message:
          "Napaka pri brisanje bolezen"
      });
    else
      res.redirect('/');
  });
});
module.exports = router;